# Enhancement/Feature description

One of the five technical pillars of Sylva is “Energy Efficiency”: by means of a tool able to measure power consumption of microservices, Sylva will be able to identify optimized or inefficient CNF in real condition and possibly implement optimization mechanism based for example on K8s scaling capabilities.

Also, power consumption measurements can be used to provide useful information in terms of workload cost and of CO2 footprint, which is becoming more and more critical.

Such a tool should implement a mechanism that collects power consumption metrics from the node components (CPU, RAM, Storage, Other…), combining those with process-related resource utilization metrics, in order to assess the real amount of energy consumed by each process.

# Technical implementation

It is proposed to add Kepler [[1](https://github.com/sustainable-computing-io/kepler)] as an integrated Sylva unit. Kepler is able to provide energy consumption metrics, in a Prometheus format, by means of exporters that are installed for each cluster nodes. Energy metrics are given at container level or at node level. Kepler can assign power to processes (i.e. containers) using a BPF program integrated into the kernel's pathway to extract process-related resource utilization metrics or use metrics from Hardware Counters or cGroups [[2](https://sustainable-computing.io/)].
It can use different approach for different scenarios [[3](https://www.cncf.io/blog/2023/10/11/exploring-keplers-potentials-unveiling-cloud-application-power-consumption/)]: in case of BM clusters with x86 power meter capabilities, it will rely on real power measurements, otherwise when power meters are not available, it will rely on pre-trained power estimation models.

Kepler is installed in a dedicated namespace on the cluster.

Kepler metrics can be scraped and visualized by the Prometheus and Grafana tools already available in Sylva clusters.


[1] https://github.com/sustainable-computing-io/kepler

[2] https://sustainable-computing.io/

[3] https://www.cncf.io/blog/2023/10/11/exploring-keplers-potentials-unveiling-cloud-application-power-consumption/